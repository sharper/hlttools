#!/usr/bin/env python
import json
import argparse

def combine_dicts(all_dicts):
    """combines dictionaries of the form [path_name][era] where era is unique but 
    path_name may or may not be repeated in each dictionary
    """
    comb_dict = {}
    for dict_ in all_dicts:
        for path in dict_:
            if path not in comb_dict:
                comb_dict[path] = {}
            for key in dict_[path]:
                comb_dict[path][key] = dict_[path][key].copy()
    return comb_dict

def make_datasets(hlt_paths,era):

    hlt_path_names = hlt_paths.keys()
    hlt_path_names.sort()

    datasets = {}
    for name,data in hlt_paths.iteritems():
        try:
            dataset =  data[era]["dataset"]
            if dataset not in datasets:
                datasets[dataset] = []
            datasets[dataset].append(name)
        except KeyError:
            pass #not all triggers will have all era
    for dataset in datasets:
        datasets[dataset].sort()
    return datasets


def make_tbl_str(hlt_paths,era,add_details_link=True):
    datasets = make_datasets(hlt_paths,era)
    dataset_names = datasets.keys()
    dataset_names.sort()

    output = []
    if add_details_link: output.append("| *path* | *act. lumi (fb-1)* | *eff. lumi (fb-1)* | *first run* | *last run* | *menu version* | *dataset* | *details* |\n ")
    else: output.append("| *path* | *act. lumi (fb-1)* | *eff. lumi (fb-1)* | *first run* | *last run* | *menu version* | *dataset* |\n ")
    for dataset in dataset_names:
        for hlt_path_name in datasets[dataset]:
            hlt_path = hlt_paths[hlt_path_name]
            if hlt_path[era]["lumi_active"]==0:
                print "HLT path ",hlt_path_name," has no active lumi"
                continue

            lumi_eff = hlt_path[era]["lumi_effective"]
            lumi_eff_format = '  {d[lumi_effective]:.2f}' if lumi_eff >= 0.1 else '  {d[lumi_effective]:.4f}'
            link_name = hlt_path_name[0:min(len(hlt_path_name),31)]
            if add_details_link: output.append(("| {name} |  {d[lumi_active]:.2f}| "+lumi_eff_format+" |  {d[first_run]} | {d[last_run]}  |  {d[menu_version]}  | {dataset}  |  [[https://twiki.cern.ch/twiki/bin/view/CMS/EgHLTPathDetails#{link_name}][details]]  |\n").format(name=hlt_path_name,d=hlt_path[era],dataset=dataset,link_name=link_name))   
            else: output.append(("| {name} |  {d[lumi_active]:.2f}| "+lumi_eff_format+" |  {d[first_run]} | {d[last_run]}  |  {d[menu_version]}  | {dataset}  |\n").format(name=hlt_path_name,d=hlt_path[era],dataset=dataset))
    return "".join(output)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='reads in various HLT data and generates a summary twiki')
    parser.add_argument('hlt_jsons',nargs="+",help='hlt data json')
    parser.add_argument('--inTwiki','-i',required=True,help='input twiki text file')
    parser.add_argument('--outTwiki','-o',required=True,help='output twiki text file')
    parser.add_argument('--details','-d',action='store_true',help='output details column')

    args = parser.parse_args()
    
    all_dicts = []
    for hlt_json in args.hlt_jsons:
        with open(hlt_json) as f:
            all_dicts.append(json.load(f))

    hlt_data = combine_dicts(all_dicts)
    
    tbl2018_str = make_tbl_str(hlt_data,"2018",args.details)
    tbl2017_str = make_tbl_str(hlt_data,"2017",args.details)
    tbl2016_str = make_tbl_str(hlt_data,"2016",args.details)
    tbl2015_25ns_str = make_tbl_str(hlt_data,"2015_25ns",args.details)
    
    
    with open(args.inTwiki) as f:
        twiki_base_str = f.read()
    with open(args.outTwiki,"w") as f:
        twiki_str = twiki_base_str.format(table2015_25ns=tbl2015_25ns_str,table2016=tbl2016_str,table2017=tbl2017_str,table2018=tbl2018_str)
        f.write(twiki_str)

