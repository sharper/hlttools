#!/usr/bin/env python


import argparse
import importlib
import json

def rm_hlt_version(name):
    version_start = name.rfind("_v")
    if version_start == -1: 
        return name
    else:
        return name[:version_start+2] 

def get_physics_datasets(streams_pset):
    """is a physics dataset if it a stream starting with 'Physics'. Additionally each dataset should only be in a single stream, we assume this and not check for dups"""
    physics_datasets = []
    for stream in streams_pset.parameters_():
        if stream.find("Physics")==0:
            physics_datasets.extend(streams_pset.parameters_()[stream].value())
    return physics_datasets
            

def fill_dataset_dict(streams_pset,datasets_pset,datasets_dict):
    physics_datasets = get_physics_datasets(streams_pset)#
#    for dataset in datasets_pset.parameters_():
    for dataset in physics_datasets:
        if dataset.find('Parking')==0 or dataset.find('Ephemeral')==0 or dataset.find('MinimumBias')==0 or dataset.find('Scouting')==0: continue #ignore the parking and ephemeral datasets
        for path in datasets_pset.parameters_()[dataset].value():
            if path.find("HLT_")!=0: continue
            path = rm_hlt_version(path)
            if path in ['HLT_L1FatEvents_v']: continue

            if path not in datasets_dict:
                datasets_dict[path] = dataset
            elif dataset != datasets_dict[path]:
#                datasets_dict[path].append(dataset)
                if path not in ["HLT_DoubleMu20_7_Mass0to30_Photon23_v","HLT_Physics_part0_v","HLT_Physics_part1_v","HLT_Physics_part2_v","HLT_Physics_part3_v","HLT_ZeroBias_part1_v","HLT_ZeroBias_part2_v","HLT_ZeroBias_part3_v"]:
                    raise RuntimeError("error, path {} was in {} but is now in {}".format(path,datasets_dict[path],dataset))
    
                

def main():

    parser = argparse.ArgumentParser(description='dumps the save tag filters of a menu')

    parser.add_argument('hlt_menus',help="the python files containing the hlt menu",nargs="+")
    parser.add_argument('--out','-o',help="output file",default='output.json')
    args = parser.parse_args()

    datasets_dict={}
    for hlt_menu in args.hlt_menus:
        with open(hlt_menu) as f:
            exec f.read()
        print "processing ",hlt_menu,process.datasets.parameterNames_()
        fill_dataset_dict(process.streams,process.datasets,datasets_dict)
        del process
        
    with open(args.out,'w') as f:
        json.dump(datasets_dict, f,sort_keys = True)

# print_filter_sel(process,"HLT_Ele27_WPTight_Gsf_v4")
if __name__ == "__main__":
    main()

