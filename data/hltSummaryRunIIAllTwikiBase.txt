<noautolink>

This twiki is a  list of the physics triggers run in 2016, 2017 and 2018.  It lists the run range they were active in, the menu version they first appeared in as well active and effective luminosities. <br>
<br>
If you are interested in E/gamma triggers, please see [[https://twiki.cern.ch/twiki/bin/view/CMS/EgHLTRunIISummary][EgHLTRunIISummary]] which is much more complete.<br>
<br>
Active lumi is the luminosity when the trigger prescale is not 0. <br>
Effective luminosity is the active luminosity times the HLT and L1 prescales.<br>
<br>
Due to paths being seeded by many L1 seeds, it is difficult to combine L1 prescales properly here the L1 prescale is simply the lowest prescale of its seeds that is non zero. There could be be accuracy issues here for edge cases so %RED%these luminosity values are simply a guide to use when picking your trigger and should not be used for detailed analysis. It is advisable to use the brilcalc trigger path luminosity feature as a cross check.%ENDCOLOR%. Please let us know if you find an issue and we can correct it (this twiki is automatically generated so any edits here will be lost next time it is uploaded)<br>
<br>
Not all triggers are written out in physics streams, for example some (mostly for the tau group) triggers are run simply to record their decision but do not accept events. In this case the dataset is "NotFound". Other examples of events not going to the normal physics streams are HLT_Physics split paths and B parking paths.
<br>


%TOC%
---+++ 2015 25ns running
Note this has problems with triggers using technical trigger bits but should not be relavent to physics triggers. It also has issues with effective lumi and menu version. HLT Physics/ ZeroBias triggers also migrate between datasets but can be ignored as not physics triggers.
{table2015_25ns}

---+++ 2016
Note L1 thresholds became high at the end of the run and may be higher than the HLT threshold
{table2016}

---+++ 2017
{table2017}

---+++ 2018
{table2018}
